# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#

# Create default user/cooker
user = User.create(email: 'admin@tractionguest.com', password: '123123')
cooker = Cooker.create(name:'Traction Guest', title:'Executive Chef', created_by: user.id)

# Create 10 fake recipes
10.times do
  cooker.recipes.create(title: Faker::StarWars.character, description: Faker::Lorem.paragraph(2, false, 2))
end


# Ailison's user
user = User.create(email: 'ailison@hotmail.com', password: '123123')
cooker = Cooker.create(name:'Ailison Rocha de Carvalho', title:'Head Chef', created_by: user.id)

# Create 10 fake recipes
10.times do
  cooker.recipes.create(title: Faker::StarWars.character, description: Faker::Lorem.paragraph(2, false, 2))
end