class ChangeCreatedByTypeToInteger < ActiveRecord::Migration[5.2]
  def up
    change_column :cookers, :created_by, 'integer USING CAST(created_by AS integer)'
  end

  def down
    change_column :cookers, :created_by, 'string USING CAST(created_by AS string)'
  end
end
