class RecipeSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers

  # attributes to be serialized
  attributes :id, :title, :description, :image, :created_at

  # model association
  belongs_to :cooker

  def image
    rails_blob_path(object.image, only_path: true) if object.image.attached?
  end

  def created_at
    object.created_at.strftime('%x %r')
  end
end
