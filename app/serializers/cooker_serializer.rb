class CookerSerializer < ActiveModel::Serializer
  # attributes to be serialized
  attributes :id, :name, :title, :contact

  # model association
  has_many :recipes

  def contact
    email = User.select(:email).find(object.created_by).email
  end
end
# Active model serializers are FAST! https://buttercms.com/blog/json-serialization-in-rails-a-complete-guide