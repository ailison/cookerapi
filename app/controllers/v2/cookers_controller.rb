module V2
  class CookersController < ApplicationController
    before_action :set_cooker, only: [:show, :update, :destroy]

    # GET example
    def index
      json_response({ message: 'Hello there'})
    end
  end
end