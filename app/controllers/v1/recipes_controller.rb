module V1
  class RecipesController < ApplicationController
    skip_before_action :authorize_request, only: [:index, :show]
    before_action :set_cooker, except: [:index, :show]
    before_action :set_cooker_recipe, only: [:update, :destroy]

    # GET /cookers/:cooker_id/recipes
    def index
      json_response(Recipe.all().order('created_at DESC'))
    end

    # GET recipes/:id
    def show
      json_response(Recipe.where(["id = ?", params[:id]]).order('created_at DESC'))
    end

    # POST /cookers/:cooker_id/recipes
    def create
      @cooker.recipes.create!(recipe_params)
      json_response(@cooker, :created)
    end

    # PUT /cookers/:cooker_id/recipes/:id
    def update
      @recipe.update(recipe_params)
      head :no_content
    end

    # DELETE /cookers/:cooker_id/recipes/:id
    def destroy
      @recipe.destroy
      head :no_content
    end


    private
    def recipe_params
      params.permit(:title, :description, :image)
    end

    def set_cooker
      @cooker = Cooker.find(params[:cooker_id])
    end

    def set_cooker_recipe
      @recipe = @cooker.recipes.find_by!(id: params[:id]) if @cooker
    end
  end
end