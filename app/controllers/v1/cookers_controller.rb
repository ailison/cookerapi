module V1
  class CookersController < ApplicationController
    before_action :set_cooker, only: [:show, :update, :destroy]

    # GET /cookers
    def index
      @cookers = current_user.cookers
      json_response(@cookers)
    end

    # POST /cookers
    def create
      @cooker = current_user.cookers.create!(cooker_params)
      json_response(@cooker, :created)
    end

    # GET /cookers/:id
    def show
      json_response(@cooker)
    end

    # PUT /cookers/:id
    def update
      @cooker.update(cooker_params)
      head :no_content
    end

    # DELETE /cookers/:id
    def destroy
      @cooker.destroy
      head :no_content
    end

    private
    # whitelist params
    def cooker_params
      params.permit(:name, :title)
    end

    def set_cooker
      @cooker = Cooker.find(params[:id])
    end
  end
end