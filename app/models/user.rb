class User < ApplicationRecord
  # encrypt password
  has_secure_password

  # Model associations
  has_many :cookers, foreign_key: :created_by

  # Validations
  validates_presence_of :email, :password_digest
  validates :email, format: {
      with: URI::MailTo::EMAIL_REGEXP,
      message: 'Email is invalid.'
  }
  validates_uniqueness_of :email

  validates :password, presence: true, confirmation: true, length: { minimum: 8 }
  validates :password, length: {minimum: 6}
end