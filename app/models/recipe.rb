class Recipe < ApplicationRecord
  after_find :treat_description
  before_destroy :delete_image

  # active storage
  has_one_attached :image

  # model association
  belongs_to :cooker

  # validation
  validates_presence_of :title, :description, :image
  validates :title, length: {minimum: 3}
  validates :description, length: {minimum: 10}
  validate :image_validation

  def treat_description
    self.description = "Here we go. #{self.description}"
  end

  def image_validation
    if image.attached?
      if image.blob.byte_size > 1000000
        image.purge
        errors[:base] << 'Image is too big, max 1Mb.'
      elsif !image.blob.content_type.starts_with?('image/')
        image.purge
        errors[:base] << 'Image is in the wrong format.'
      end
    else
      errors[:base] << 'Image isn\'t attached.'
    end
  end

  def delete_image
    self.image.purge
  end
end
