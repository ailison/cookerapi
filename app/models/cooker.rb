class Cooker < ApplicationRecord
  # model association
  has_many :recipes, dependent: :destroy

  # validations
  validates_presence_of :name, :title, :created_by
  validates :name, length: {minimum: 3}
  validates :title, length: {minimum: 4, maximum: 9}
end