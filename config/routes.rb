Rails.application.routes.draw do
  # NON DEFAULT VERSIONS
  scope module: :v2, constraints: ApiVersion.new('v2') do
    resources :cookers, only: :index
  end

  # DEFAULT VERSION
  # namespace controllers without affecting the URI
  scope module: :v1, constraints: ApiVersion.new('v1', true) do
    resources :cookers do
      resources :recipes, only: [:create, :update, :destroy] # To list recipes with GET cookers/:cooker_id (serializer magic)
    end

    # public URLs
    get 'recipes/', to: 'recipes#index'
    get 'recipes/:id', to: 'recipes#show'
  end

  # authentication url
  post 'auth/login', to: 'authentication#authenticate'
  # post 'signup', to: 'users#create' # If the API had a free signup for anyone.
end