FactoryBot.define do
  factory :recipe do
    title { Faker::StarWars.character } # NERDY :)
    description { Faker::Lorem.sentence(50, false, 20) }
    image { Rack::Test::UploadedFile.new('/home/ailison/Pictures/spanishrice.jpg', 'image/jpg') }
    cooker_id { nil }
  end
end