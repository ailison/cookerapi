FactoryBot.define do
  factory :cooker do
    name { Faker::Name.name }
    title { 'Sous Chef' }
    created_by { Faker::Number.number(1) }
  end
end