FactoryBot.define do
  factory :user do
    email { Faker::Internet::email }
    password { Faker::Internet::password(8,10,true,true) }
  end

  factory :github do
    Faker::Omniauth.github
  end
end