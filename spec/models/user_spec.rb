require 'rails_helper'

# Test suite for User model
RSpec.describe User, type: :model do
  # Association test
  it { should have_many(:cookers) }

  # Validation tests
  describe 'Validations' do
    context 'on a new user' do
      it 'should not be valid without a password' do
        user = User.new password: nil, password_confirmation: nil
        should_not be_valid(user)
      end

      it 'should be not be valid with a short password' do
        user = User.new password: 'short', password_confirmation: 'short'
        should_not be_valid(user)
      end

      it 'should not be valid with a confirmation mismatch' do
        user = User.new password: 'short', password_confirmation: 'long'
        should_not be_valid(user)
      end
    end

    context 'on an existing user' do
      let(:user) do
        user = User.create(email: 'anything@valid.com', password: 'password*', password_confirmation: 'password*')
      end

      it 'should not be valid with an empty password' do
        user.password = user.password_confirmation = ''
        should_not be_valid(user)
      end

      it 'should be valid with a new (valid) password' do
        user.password = user.password_confirmation = 'new password'
        should_not be_valid(user)
      end

      it 'should validate the presence of password' do
        should validate_presence_of(:password)
      end

      it 'should validate the presence of email' do
        should validate_presence_of(:email)
      end

      it 'should validate the uniqueness of email' do
        should validate_uniqueness_of(:email)
      end

      it 'should not allow invalid email' do
        should_not allow_value('im smart').for(:email)
      end

      it 'should allow valid email' do
        should allow_value('valid@email.com').for(:email)
      end
    end
  end
end