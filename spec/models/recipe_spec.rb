require 'rails_helper'

# Test suite for the Recipe model
RSpec.describe Recipe, type: :model do
  # Association test
  it { should belong_to(:cooker) }

  # Validation test
  it { should validate_presence_of(:title) }
  it { should validate_length_of(:title).is_at_least(3)}

  it { should validate_presence_of(:description) }
  it { should validate_length_of(:description).is_at_least(10)}
end