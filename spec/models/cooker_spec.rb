require 'rails_helper'

# Test suite for the Cooker model
RSpec.describe Cooker, type: :model do
  # Association test ensure Cooker model has a 1:m relationship with the Item model
  it { should have_many(:recipes).dependent(:destroy) }

  # Validation tests
  it { should validate_presence_of(:name) }
  it { should validate_length_of(:name).is_at_least(3)}

  it { should validate_presence_of(:title) }
  it { should validate_length_of(:title).is_at_least(4)}
  it { should validate_length_of(:title).is_at_most(9)}

  it { should validate_presence_of(:created_by) } # week point

  # Equivalent to the REQUESTS/"POST /cookers"
  it "is valid with valid attributes" do
    expect(Cooker.new(name: 'Testao', title: 'Yahoo', created_by: 1)).to be_valid
  end
end