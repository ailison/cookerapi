require 'rails_helper'

RSpec.describe 'Cookers API', type: :request do
  # add cookers owner
  let(:user) { create(:user) }
  let!(:cookers) { create_list(:cooker, 10, created_by: user.id) }
  let(:cooker_id) { cookers.first.id }
  # authorize request
  let(:headers) { valid_headers }

  describe 'GET /cookers' do
    # update request with headers
    before { get '/cookers', params: {}, headers: headers }

    it 'returns cookers' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /cookers/:id' do
    before { get "/cookers/#{cooker_id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the cooker' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(cooker_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:cooker_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Cooker/)
      end
    end
  end

  describe 'POST /cookers' do
    let(:valid_attributes) do
      # send json payload
      { name: 'Jonas', title: 'Gnome', created_by: user.id.to_s }.to_json
    end

    context 'when request is valid' do
      before { post '/cookers', params: valid_attributes, headers: headers }

      it 'creates a cooker' do
        expect(json['title']).to eq('Gnome')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      let(:invalid_attributes) { { title: nil }.to_json }
      before { post '/cookers', params: invalid_attributes, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(json['message']).to match(/Validation failed: Name can't be blank, /)
      end
    end
  end

  describe 'PUT /cookers/:id' do
    let(:valid_attributes) { { title: 'Prep' }.to_json }

    context 'when the record exists' do
      before { put "/cookers/#{cooker_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /cookers/:id' do
    before { delete "/cookers/#{cooker_id}", params: {}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end