require 'rails_helper'

RSpec.describe 'Recipes API' do
  let(:user) { create(:user) }
  let!(:cooker) { create(:cooker, created_by: user.id) }
  let!(:recipes) { create_list(:recipe, 20, cooker_id: cooker.id) }
  let(:cooker_id) { cooker.id }
  let(:id) { recipes.first.id }
  let(:headers) { valid_headers }
=begin
# Comented tests that URL route are no longer available
  describe 'GET /cookers/:cooker_id/recipes' do
    before { get "/cookers/#{cooker_id}/recipes", params: {}, headers: headers }

    context 'when cooker exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all cooker recipes' do
        expect(json.size).to eq(20)
      end
    end

    context 'when cooker does not exist' do
      let(:cooker_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Cooker/)
      end
    end
  end

  describe 'GET /cookers/:cooker_id/recipes/:id' do
    before { get "/cookers/#{cooker_id}/recipes/#{id}", params: {}, headers: headers }

    context 'when cooker recipe exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the recipe' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when cooker recipe does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Recipe/)
      end
    end
  end
=end
  describe 'POST /cookers/:cooker_id/recipes' do
    let(:valid_attributes) { { title: 'Visit Narnia', description: 'Narnia descript' }.to_json }

    context 'when request attributes are valid' do
      before do
        post "/cookers/#{cooker_id}/recipes", params: valid_attributes, headers: headers
      end

      it 'but miss the image attachment and returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Image isn't attached./)
      end
    end

    context 'when an invalid request' do
      before { post "/cookers/#{cooker_id}/recipes", params: {}, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Title can't be blank/)
      end
    end
  end

  describe 'PUT /cookers/:cooker_id/recipes/:id' do
    let(:valid_attributes) { { title: 'Popeye' }.to_json }

    before do
      put "/cookers/#{cooker_id}/recipes/#{id}", params: valid_attributes, headers: headers
    end

    context 'when recipe exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the recipe' do
        updated_recipe = Recipe.find(id)
        expect(updated_recipe.title).to match(/Popeye/)
      end
    end

    context 'when the recipe is' do
      it 'attached a file' do
        updated_recipe = Recipe.find(id)
        updated_recipe.image.attach(io: File.open("/home/ailison/Pictures/spanishrice.jpg"), filename: "spanishrice.jpg", content_type: "image/jpg")
        expect(updated_recipe.image.attached?).to be_truthy
      end
    end

    context 'when recipe does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Recipe/)
      end
    end
  end

  describe 'DELETE /cookers/:id' do
    before { delete "/cookers/#{cooker_id}/recipes/#{id}", params: {}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end